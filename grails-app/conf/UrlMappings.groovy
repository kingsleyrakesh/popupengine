class UrlMappings {

	static mappings = {
		
		"/outboundCalls"(resources:"outboundCall")
		
		"/getNextOutbound" (controller: "outboundCall", action: "getNextOutbound" )
		
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
