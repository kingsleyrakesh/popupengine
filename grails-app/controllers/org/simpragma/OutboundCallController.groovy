package org.simpragma

import grails.converters.JSON
import grails.rest.RestfulController

class OutboundCallController extends RestfulController {

	OutboundCallController() {
		super(OutboundCall)
	}

	def getNextOutbound() {

		if(params.apikey && params.apikey == grailsApplication.config.popup.apikey){
			println "apikey match"
		} else {
			println "apikey do not match"
			render status : 404
			return
		}
		Map<String, Object> finalMap = new HashMap<String, Object>()
		if(params.empId) {
			def ocCriteria = OutboundCall.createCriteria()
			def ocList = ocCriteria.list() {
				isNull("empId")
				eq("isAssigned", false)
				eq("isExpired", false)
				order("addedDate", "desc")
			}
			if(ocList.size() > 0) {
				ocList.get(0).empId = params.empId
				ocList.get(0).isAssigned = true
				ocList.get(0).save(flush: true, failOnError: true)
				finalMap.put("status", 200)
				finalMap.put("data", ocList.get(0))
				render finalMap as JSON
			} else {
			finalMap.put("status", 200)
			finalMap.put("data", [])
			render finalMap as JSON
			}
		} else {
			finalMap.put("status", 404)
			finalMap.put("data", [])
			render finalMap as JSON
		}
	}
	
}
