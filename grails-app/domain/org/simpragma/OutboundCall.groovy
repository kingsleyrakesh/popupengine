package org.simpragma

class OutboundCall {
	
	String fileReferenceNumber
	Long addedDate
	Long assignedDate
	String empId
	Boolean isExpired
	Boolean isAssigned
	
	static constraints = {
		
		fileReferenceNumber nullable : true
		addedDate nullable: true
		assignedDate nullable: true
		empId nullable: true
		isExpired nullable: true
		isAssigned nullable: true
		
	}
	
}